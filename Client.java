public class Client{
	public static void main(String[] args){
		Calc calc = new Calc();
		calc.register("*", new Multiplicacion());
		calc.register("/", new Division());
		
		int result = calc.operate("+",2,5);
		System.out.println(result);
		int result1 = calc.operate("-",5,3);
		System.out.println(result1);
		int result2 = calc.operate("*",5,3);
		System.out.println(result2);
		int result3 = calc.operate("@",2,4);
		System.out.println(result3);
	}
}



class Multiplicacion extends Operation{
	Integer operate(Integer a, Integer b){
		return a*b;
	}
}

class Division extends Operation{
	Integer operate(Integer a, Integer b){
		return a/b;
	}
}

