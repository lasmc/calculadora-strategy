import java.util.*;

public class Calc{
	Map<String, Operation> operations;
	
	public Calc(){
		operations = new HashMap<String, Operation>();
		register("+", new Sum());
		register("-", new Substraction());
		register("@", new Arroba());
		
	}
	
	public void register(String operator, Operation operation){
		operations.put(operator, operation);
	}
	
	public int operate(String operator, Integer a , Integer b){
		Operation operation = operations.get(operator);
		return operation.operate(a,b);
	}
}

abstract class Operation{ //estrategia
	abstract Integer operate(Integer a , Integer b);
}


class Sum extends Operation{
	Integer operate(Integer a, Integer b){
		return a+b;
	}
}

class Substraction extends Operation{
	Integer operate(Integer a, Integer b){
		return a-b;
	}
}

class Arroba extends Operation{
	Integer operate(Integer a, Integer b){
		return (b*(a-2))/(a+b);
	}
}